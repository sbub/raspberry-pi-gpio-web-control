#!/bin/bash

# Copyright 2012 der-bub.de
# http://www.der-bub.de
# Author: Sebastian Bub (sebastian@der-bub.de)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

DIR=`dirname "$0"`
cd "$DIR"

#### CHANGE YOUR JAVA_HOME
JAVA_HOME=/usr/lib/jvm/jdk-7-oracle-arm-vfp-hflt/

#### CHECK VALUE of WINSTONE_JAR
WINSTONE_JAR=./winstone-0.9.10.jar
WINSTONE_LOG=logs/winstone.log

export JAVA_HOME
PATH="$JAVA_HOME/bin:$PATH"
export PATH

WARFILE=target/raspberry-pi-gpio-web-control-1.0-SNAPSHOT.war
GPIO_CONFIG=gpio.conf
CRON_CONFIG=cron.conf
CMD_CONFIG=cmd.conf
RPI_GPIO_LOG=file:`pwd`/log4j.properties

# remote debugging parameter java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 \

# for automated testing, overwrite gpio.conf and cmd.conf (just uncomment)
# GPIO_CONFIG=src/test/resources/gpio.conf
# CMD_CONFIG=src/test/resources/cmd.conf

java \
          -Dlog4j.configuration=$RPI_GPIO_LOG \
          -Dgpio.configuration=$GPIO_CONFIG \
          -Dcron.configuration=$CRON_CONFIG \
          -Dcmd.configuration=$CMD_CONFIG \
		  -jar $WINSTONE_JAR \
		  --ajp13Port=-1 \
		  --warfile=$WARFILE \
		  --debug=6 \
		  --logfile=$WINSTONE_LOG \
#		  --useJasper \
#		  --commonLibFolder=target/raspberry-pi-gpio-web-control-1.0-SNAPSHOT/WEB-INF/lib/ \
#		  --toolsJar=/Users/sb/bak/rootServer/sbhome/j2sdk1.4.2_19/lib/tools.jar \

# in case you want to use JSPs, uncomment the above lines and in pom.xml, too

cd -

