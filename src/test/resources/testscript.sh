#!/bin/bash

# Copyright 2013 der-bub.de
# http://www.der-bub.de
# Author: Sebastian Bub (sebastian@der-bub.de)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################

# simple curl test script which must conform to src/test/resources/gpio.conf

# GPIO.0.DIRECTION=OUT
# GPIO.0.NAME=OUTg0d0t250b1000
# GPIO.0.OUT.DEFAULT.STATE=0
# GPIO.0.OUT.AUTO.TOGGLE.TIME=250
# GPIO.0.OUT.BLOCK.TIME=1000
#
# GPIO.1.DIRECTION=OUT
# GPIO.1.NAME=OUTg1
#
# GPIO.4.DIRECTION=IN
# GPIO.4.NAME=INg4
#
# GPIO.7.DIRECTION=TI
# GPIO.7.NAME=TIg7
# GPIO.7.OUT.DEFAULT.STATE=0
# GPIO.7.OUT.AUTO.TOGGLE.TIME=250
# GPIO.7.OUT.BLOCK.TIME=1000

CURL=curl
HOST=localhost
PORT=8080

SUCCESSCOUNTER=0
FAILURECOUNTER=0


singletest() {
	CMD=$1
	EXPECTED=$2
	RESPONSE="`$CURL -s http://$HOST:$PORT/handle?$CMD`"
	if [ "$RESPONSE" == "$EXPECTED" ]; then
		echo "SUCCESS for $CMD - was $RESPONSE"
		SUCCESSCOUNTER=$(( $SUCCESSCOUNTER + 1 ))
	else
		echo "FAILED for $CMD - was $RESPONSE --- expected $EXPECTED"
		FAILURECOUNTER=$(( $FAILURECOUNTER + 1 ))
	fi
}

# read unset output port
singletest 'OUTg1=IN' '{"OUTg1":-1}'
# set output port
singletest 'OUTg1=1' '{"OUTg1":1}'
# read output port
singletest 'OUTg1=IN' '{"OUTg1":1}'
# set output port
singletest 'OUTg1=1' '{"OUTg1":1}'
# set output port with other value
singletest 'OUTg1=0' '{"OUTg1":0}'
# read output port
singletest 'OUTg1=IN' '{"OUTg1":0}'
# set output port with other value
singletest 'OUTg1=1' '{"OUTg1":1}'
# read unset virtual variable
singletest 'VIRTUALt1=IN' '{"VIRTUALt1":0}'
# set virtual variable
singletest 'VIRTUALt1=1' '{"VIRTUALt1":1}'
# read virtual variable
singletest 'VIRTUALt1=IN' '{"VIRTUALt1":1}'
# set virtual variable other value
singletest 'VIRTUALt1=0' '{"VIRTUALt1":0}'
# read virtual variable
singletest 'VIRTUALt1=IN' '{"VIRTUALt1":0}'
# conditions on output, set virtual variable
singletest 'OUTg1==1&VIRTUALt1=1' '{"VIRTUALt1":1}'
singletest 'OUTg1!=1&VIRTUALt1=1' '{}'
singletest 'OUTg1!=0&VIRTUALt1=1' '{"VIRTUALt1":1}'
singletest 'OUTg1>=1&VIRTUALt1=1' '{"VIRTUALt1":1}'
singletest 'OUTg1>1&VIRTUALt1=1' '{}'
singletest 'OUTg1<=1&VIRTUALt1=1' '{"VIRTUALt1":1}'
singletest 'OUTg1<1&VIRTUALt1=1' '{}'
# conditions on output, set output port
singletest 'OUTg1==1&OUTg8=1' '{"OUTg8":1}'
singletest 'OUTg1!=1&OUTg8=1' '{}'
# conditions on virtual variable, set virtual variable
singletest 'VIRTUALt1==1&VIRTUALt2=1' '{"VIRTUALt2":1}'
singletest 'VIRTUALt1!=1&VIRTUALt2=1' '{}'
# conditions on virtual variable, set output port
singletest 'VIRTUALt1==1&OUTg8=1' '{"OUTg8":1}'
singletest 'VIRTUALt1!=1&OUTg8=1' '{}'
# check blocking time
singletest 'OUTg0d0t250b1000=1' '{"OUTg0d0t250b1000":1}'
singletest 'OUTg0d0t250b1000=0' '{"OUTg0d0t250b1000":-2}'
sleep 1
singletest 'OUTg0d0t250b1000=0' '{"OUTg0d0t250b1000":0}'
sleep 1
# check toggle time
singletest 'OUTg0d0t250b1000=1' '{"OUTg0d0t250b1000":1}'
singletest 'OUTg0d0t250b1000=IN' '{"OUTg0d0t250b1000":1}'
sleep 1
singletest 'OUTg0d0t250b1000=IN' '{"OUTg0d0t250b1000":0}'
# write input port (which does not work)
singletest 'INg4=1' '{}'
# read input port (simulation returns 2)
singletest 'INg4=IN' '{"INg4":2}'
# conditions on input, set virtual variable
singletest 'INg4==2&VIRTUALt2=1' '{"VIRTUALt2":1}'
singletest 'INg4!=2&VIRTUALt2=1' '{}'
# conditions on input, set output port
singletest 'INg4==2&OUTg8=1' '{"OUTg8":1}'
singletest 'INg4!=2&OUTg8=1' '{}'
# read uninitialized value of a command
singletest 'CMDsleeper1=IN' '{"CMDsleeper1":0}'
# check on an uninitialized value of a command
singletest 'CMDsleeper2==0&VIRTUALt3=1' '{"VIRTUALt3":1}'
# execute two commands
singletest 'CMDsleeper1=1&CMDsleeper2=1' '{"CMDsleeper1":2,"CMDsleeper2":0}'
singletest 'CMDsleeper2=1&CMDsleeper1=1' '{"CMDsleeper1":2,"CMDsleeper2":0}'
# execute a command and check on it successful
singletest 'CMDsleeper1=0&CMDsleeper1==1&VIRTUALt3=2' '{"CMDsleeper1":1,"VIRTUALt3":2}'
# execute a command and check on it successful
singletest 'CMDsleeper1=0&CMDsleeper1==7&VIRTUALt3=2' '{"CMDsleeper1":1}'
# execute a command and check on it unsuccessful
singletest 'CMDsleeper1=0&CMDsleeper1==7&VIRTUALt3=2' '{"CMDsleeper1":1}'
# execute a command which runs into timeout and check on it unsuccessful
singletest 'CMDsleeper2=2&CMDsleeper2==0&VIRTUALt3=2' '{"CMDsleeper2":143}'


echo "SUCCESS: $SUCCESSCOUNTER - FAILURE: $FAILURECOUNTER"
