## Here are all resources for automated tests

The automated tests are only run in simulation mode. Do not change `src/test/resources/gpio.conf` and do not change `src/test/resources/cmd.conf` because it's configuration has all different gpio port configurations and external commands and is checked from the `src/test/resources/testscript.sh`.

In `start.sh` there are comments to be used for configuration.

### Testing

In order to run the test:

* uncomment the testlines in `start.sh`
* start the server
* run the testscript (it expects a curl in it's $PATH) `src/test/resources/testscript.sh`
* stop the server
* (running the testscript multiple times will show failures (because of tests of uninitialized values))

