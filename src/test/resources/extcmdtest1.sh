#!/bin/bash

# Copyright 2013 der-bub.de
# http://www.der-bub.de
# Author: Sebastian Bub (sebastian@der-bub.de)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################

# set the parameter (just as an example)
PARAM=$1

echo test on stdout, param was $PARAM
echo test on stderr >&2

# sleep a little
sleep $PARAM

# return parameter+1
exit  $[ PARAM + 1 ]

