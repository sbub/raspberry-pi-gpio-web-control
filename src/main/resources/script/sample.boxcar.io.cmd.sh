#!/bin/bash

# Copyright 2012 der-bub.de
# http://www.der-bub.de
# Author: Sebastian Bub (sebastian@der-bub.de)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# This script is an example on how to send notifications to iPhone and Android smartphones.
#
# It is based on a service call http://boxcar.io
#
# Before you go on there, please install cURL first with the command
#             sudo apt-get install curl
#
# In order to use the script, simply "Join" on their website and use the bottom navigation
# to create your own Provider (click "Provider API). After creating they show an example URL,
# and your API key (NOT the API secret). The API key must be set below and your email address
# you have used for joining boxcar.io.
#
# Install their App on your device and allow notifications.


# the key is provided by boxcar.io after you have created your provider (NOT the API secret)
API_KEY=xxx

# same email address you have used for joining boxcar.io
EMAIL_ADRESS=xxx@xxx.xxx


# set headline and message depending on the parameter you give to the script
case $1 in
1)
	HEADLINE="Notification 1"
	MSG="A notification msg 1"
	;;
2)
	HEADLINE="other Notification 2"
	MSG="An other notification msg 2"
	;;
*)
	# default headline and message
	HEADLINE="Raspberry"
	MSG="A message from Raspberry Pi GPIO Web Control"
esac

###############################

# curl executable
CURL_EXE=/usr/bin/curl
# maximum time for the request
MAX_TIME=2



$CURL_EXE -d "email=$EMAIL_ADRESS"                  \
     -d "&notification[from_screen_name]=$HEADLINE" \
     -d "&notification[message]=$MSG"               \
     -m $MAX_TIME -f                                \
     http://boxcar.io/devices/providers/$API_KEY/notifications > /dev/null 2>&1

exit $?


