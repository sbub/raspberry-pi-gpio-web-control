#!/bin/bash

# Copyright 2012 der-bub.de
# http://www.der-bub.de
# Author: Sebastian Bub (sebastian@der-bub.de)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

OLDDIR=`pwd`
DIR=`dirname "$0"`

cd "${DIR}"
VERSION=`LC_ALL=en_US hg summary| grep parent | awk '{print $2}'| sed 's/.*://'`
export VERSION
echo creating tgz for version $VERSION
echo
cd -

# (I know that you might get in trouble if project directory is sym linked
cd "${DIR}/../../../../.."

echo "This is version $VERSION of project https://bitbucket.org/sbub/raspberry-pi-gpio-web-control" >  raspberry-pi-gpio-web-control/VERSION

tar czvf raspberry-pi-gpio-web-control-${VERSION}.tgz \
    raspberry-pi-gpio-web-control/VERSION \
    raspberry-pi-gpio-web-control/README.md \
    raspberry-pi-gpio-web-control/cron.conf.MAY_BE_CHANGED \
    raspberry-pi-gpio-web-control/gpio.conf.MUST_BE_CHANGED \
    raspberry-pi-gpio-web-control/cmd.conf.MAY_BE_CHANGED \
    raspberry-pi-gpio-web-control/log4j.properties \
    raspberry-pi-gpio-web-control/logs/_doNotDelete \
    raspberry-pi-gpio-web-control/src/main/resources/init.d/gpio-winstone \
    raspberry-pi-gpio-web-control/start.sh \
    raspberry-pi-gpio-web-control/stop.sh \
    raspberry-pi-gpio-web-control/target/raspberry-pi-gpio-web-control-1.0-SNAPSHOT.war

rm raspberry-pi-gpio-web-control/VERSION

cd "$OLDDIR"

