/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio;

/**
 * Class represents the direction of a gpio pin. In and OUT are simple input or output ports.
 * TI is a "Timebased analog Input port". Read about it in gpio.conf or README.md
 *
 * @author sb
 */
public enum Direction {

    IN("in"), OUT("out"), TI("ti");
    private String directionString;

    public static final String CONDITIONAL_INPUT = "CIN";

    private Direction(String directionString) {
        this.directionString = directionString;
    }

    public String getValue() {
        return directionString;
    }

    public String getDirectionStringLine() {
        return getValue() + String.format("%n");
    }

    public boolean isInput() {
        return "in".equals(directionString);
    }

    public boolean isOutput() {
        return "out".equals(directionString);
    }

    public boolean isTimeBasedAnalogIn() {
        return "ti".equals(directionString);
    }

    public static Direction getDirectionByString(String s) {
        if (null != s) {
            String trimmedString = s.trim();
            for (Direction direction : Direction.values()) {
                if (trimmedString.equalsIgnoreCase(direction.getValue())) {
                    return direction;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return directionString;
    }
}
