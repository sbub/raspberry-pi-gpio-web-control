/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio;

import org.apache.log4j.Logger;

/**
 * Class represents a configuration for a gpio pin. Therefore it has a reference
 * to GpioPin and a Direction and for each possible configuration property a
 * variable
 *
 * @author sb
 */
public class ConfiguredGpio {

    private static Logger log = Logger.getLogger(ConfiguredGpio.class);
    public static final int NO_STATE = -1;
    public static final int BLOCKED_STATE = -2;
    public static final long NO_DEFAULT_TIME = -1L;
    private GpioPin gpioPin;
    private Direction direction;
    private String userdefinedName;
    private int defaultState = NO_STATE;
    private long autoToggleTime = NO_DEFAULT_TIME;
    private long blockTime = NO_DEFAULT_TIME;

    public ConfiguredGpio(GpioPin gpioPin, Direction direction, String name,
                          String defaultState, String autoToggleTime, String blockTime) {
        this.gpioPin = gpioPin;
        this.direction = direction;
        this.userdefinedName = name;
        setDefaultState(defaultState);
        setBlockTime(blockTime);
        // auto toggle requires defaultState and blockTime > autoToggleTime
        setAutoToggleTime(autoToggleTime);

        if (Direction.TI.equals(direction)) {
            // autoToggleTime and blockTime and defaultState are required
            if (ConfiguredGpio.NO_DEFAULT_TIME == this.autoToggleTime
                || ConfiguredGpio.NO_DEFAULT_TIME == this.blockTime
                || ConfiguredGpio.NO_STATE == this.defaultState) {
                this.direction = Direction.IN;
                this.autoToggleTime = ConfiguredGpio.NO_DEFAULT_TIME;
                this.blockTime = ConfiguredGpio.NO_DEFAULT_TIME;
                this.defaultState = ConfiguredGpio.NO_STATE;
                log.error("Auto Toggle and Block time and Default state required for Direction TI. Reconfigured as IN for " + gpioPin);
            }
        }
    }

    public GpioPin getGpioPin() {
        return gpioPin;
    }

    public Direction getDirection() {
        return direction;
    }

    public String getUserdefinedName() {
        return userdefinedName;
    }

    public int getDefaultState() {
        return defaultState;
    }

    public String getDefaultStateAsStringLine() {
        return Integer.toString(defaultState) + String.format("%n");
    }

    public long getAutoToogleTime() {
        return autoToggleTime;
    }

    public long getBlockTime() {
        return blockTime;
    }

    /**
     * methods sets default state
     *
     * @param s String the default state to use must be "0" or "1"
     */
    private void setDefaultState(String s) {
        if (direction.isInput()) {
            log.warn("Default state not set for " + gpioPin);
            return;
        }
        if (null != s) {
            String trimmedValue = s.trim();
            if ("0".equals(trimmedValue)) {
                defaultState = 0;
            } else {
                if ("1".equals(trimmedValue)) {
                    defaultState = 1;
                } else {
                    log.warn("Default state not set for " + gpioPin);
                }
            }
        }
    }

    private void setBlockTime(String s) {
        if (direction.isInput()) {
            log.debug("Block time not set for " + gpioPin);
            return;
        }
        long tmpLong = getLongTimeFromString(s);
        if (tmpLong > 0) {
            blockTime = tmpLong;
        }
    }

    private void setAutoToggleTime(String s) {
        if (direction.isInput()) {
            log.warn("Auto Toggle not set for " + gpioPin);
            return;
        }
        if (defaultState != ConfiguredGpio.NO_STATE) {
            long tmpLong = getLongTimeFromString(s);
            if (tmpLong < blockTime && tmpLong > 0) {
                autoToggleTime = tmpLong;
            } else {
                log.warn("Auto Toggle not set (block time to small or time=0) for " + gpioPin);
            }
        } else {
            log.warn("Auto Toggle not set for " + gpioPin);
        }
    }

    private long getLongTimeFromString(String s) {
        if (null != s) {
            String trimmedValue = s.trim();
            try {
                return Long.parseLong(trimmedValue);
            } catch (NumberFormatException e) {
                log.error("NumberFormatException for " + gpioPin);
            }
        }
        return NO_DEFAULT_TIME;
    }

    @Override
    public String toString() {
        return "[" + gpioPin + ":" + direction + ":" + userdefinedName
            + (NO_STATE == getDefaultState() ? "" : " default:" + getDefaultState())
            + (NO_DEFAULT_TIME == getAutoToogleTime() ? "" : " toggle:" + getAutoToogleTime())
            + (NO_DEFAULT_TIME == getBlockTime() ? "" : " block:" + getBlockTime())
            + "]";
    }
}
