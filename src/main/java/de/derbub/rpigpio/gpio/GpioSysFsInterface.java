/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio;

import de.derbub.rpigpio.gpio.files.FileHandlingFactory;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * this class writes and reads to/from the gpio pins using Sysfs
 *
 * @author sb
 */
public class GpioSysFsInterface extends GpioSysFsInitializer {

    private static Logger log = Logger.getLogger(GpioSysFsInterface.class);
    private static GpioSysFsInterface INSTANCE;
    private Map<ConfiguredGpio, Long> blockedMap = new HashMap<ConfiguredGpio, Long>();
    private static int tiMaxCounter = 100000;

    protected GpioSysFsInterface() {
        /*
         * Singleton
         */
    }

    /**
     * methods creates a singleton
     *
     * @param configuredNameGpioMapParam Map with the configured ports
     * @param tiMaxCounterParam          max counter for read loop for direction TI
     * @return a single instance of GpioSysFsInterface or throws a RuntimeException if called twice
     */
    public static GpioSysFsInterface getInstance(Map<String, ConfiguredGpio> configuredNameGpioMapParam, int tiMaxCounterParam) {
        if (INSTANCE == null) {
            INSTANCE = new GpioSysFsInterface();
            configuredGpioMap = configuredNameGpioMapParam;
            tiMaxCounter = tiMaxCounterParam;
            return INSTANCE;

        }
        throw new RuntimeException("GpioSysFsInterface already initialized");
    }

    /**
     * method returns an initialized instance only
     *
     * @return a single instance of GpioSysFsInterface which has been created using method getInstance
     */
    public static GpioSysFsInterface getInitializedInstance() {
        if (INSTANCE == null) {
            throw new RuntimeException("GpioSysFsInterface has not been initialized");
        }
        return INSTANCE;
    }

    /**
     * method will set or read gpioports
     *
     * @param nameCheckedValuesMap must contain only valid configured names and
     *                             valid values
     * @return Map consists of names as keys and for value, either set value to
     *         set or -1 if it failed or the value of IN
     */
    public synchronized Map<String, String> handleGpioPorts(Map<String, String> nameCheckedValuesMap) {
        log.debug("handleGpioPorts( " + nameCheckedValuesMap.size() + " ports )");
        Map<String, String> returnMap = new HashMap<String, String>();

        Map<ConfiguredGpio, String> batchedGpioOutMap = new HashMap<ConfiguredGpio, String>();
        for (String name : nameCheckedValuesMap.keySet()) {
            ConfiguredGpio gpio = configuredGpioMap.get(name);
            log.debug("handleGpioPorts working on " + gpio + " value: " + nameCheckedValuesMap.get(name));
            if (gpio.getDirection().isInput()) {
                String readValue = readGpio(gpio);
                returnMap.put(name, readValue);
            } else {
                // if real port state is checked (output port or TI)
                if (Direction.IN.equals(Direction.getDirectionByString(nameCheckedValuesMap.get(name)))) {
                    returnMap.put(name, (null != currentOutputPortState.get(gpio)
                        ? currentOutputPortState.get(gpio)
                        : Integer.toString(ConfiguredGpio.NO_STATE)));
                } else {
                    if (gpio.getDirection().isOutput()) {
                        // save port in map for later writing
                        batchedGpioOutMap.put(gpio, nameCheckedValuesMap.get(name));
                    }
                    if (gpio.getDirection().isTimeBasedAnalogIn()) {
                        // TI is read (and written to currentOutputPortState)
                        String readValue = readTimeBasedAnalogIn(gpio);
                        returnMap.put(name, readValue);
                    }
                }
            }
        }
        if (!batchedGpioOutMap.isEmpty()) {
            returnMap.putAll(writeGpios(batchedGpioOutMap, false));
        }
        return returnMap;
    }

    /**
     * methods resets all gpio ports to 0, afterwards it unexports the gpios and
     * removes the singleton instance.
     *
     * @return boolean success
     */
    public boolean closeAllGpioPorts() {
        boolean isReset = resetAllState();
        boolean isUnexport = unExportAllGpioPorts();
        INSTANCE = null;
        return isReset && isUnexport;
    }

    /**
     * method handles timeBasedAnalogIn. See gpio.conf for exact handling
     *
     * @param gpio ConfiguredGpio must be of Direction TI (no checking here any more)
     * @return the value read or blocked state info
     */
    private synchronized String readTimeBasedAnalogIn(ConfiguredGpio gpio) {
        String returnValue = Integer.toString(ConfiguredGpio.NO_STATE);
        Long nowForBlocking = System.currentTimeMillis();
        log.debug("readTimeBasedAnalogIn for " + gpio);

        // 0. Check that it is not blocked.
        if (isBlocked(gpio, nowForBlocking)) {
            return Integer.toString(ConfiguredGpio.BLOCKED_STATE);
        }

        //  1. It is reconfigured to be an output port.
        if (!setDirection(gpio, Direction.OUT)) {
            log.error("Could not set output direction on " + gpio);
            return returnValue;
        }
        // 2. It is set to the value of DEFAULT.STATE
        Map<ConfiguredGpio, String> outMap = new HashMap<ConfiguredGpio, String>(1);
        outMap.put(gpio, Integer.toString(gpio.getDefaultState()));
        Map<String, String> outWrittenMap = writeGpios(outMap, true); // handle as if it is isAutoTogglingThread (blocking is not checked)
        if (Integer.toString(ConfiguredGpio.NO_STATE).equals(outWrittenMap.get(gpio.getUserdefinedName()))) {
            log.error("Could not write default state value to " + gpio);
            return returnValue;
        }
        // 3. Wait for the time specified in AUTO.TOGGLE
        try {
            Thread.sleep(gpio.getAutoToogleTime());
        } catch (InterruptedException e) {
            log.error("sleep interrupted (shutdown in progress?).");
            return returnValue;
        }
        // 4. It is reconfigured to be an input port.
        if (!setDirection(gpio, Direction.IN)) {
            log.error("Could not set input direction on " + gpio);
            return returnValue;
        }
        // 5. It is read (in a while loop as fast as possible), until a value of (required) !DEFAULT.STATE is returned.
        RandomAccessFile aGpioRandomAccessFile = null;
        File aGpioFile = null;
        int defaultState = Integer.toString(gpio.getDefaultState()).charAt(0);
        int currentState;
        long counter = 0L;
        synchronized (lockObject) {
            try {
                aGpioFile = FileHandlingFactory.getFile(SYSFS_GPIO_FILE_PREFIX + gpio.getGpioPin().getNumber() + SYSFS_GPIO_VALUE_FILE_SUFFIX);
                if (aGpioFile.exists()) {
                    aGpioRandomAccessFile = FileHandlingFactory.getRandomAccessFile(aGpioFile, "r");
                    long before = System.currentTimeMillis();
                    for (int i = 0; i < tiMaxCounter; i++) {
                        aGpioRandomAccessFile.seek(0);
                        currentState = aGpioRandomAccessFile.read();
                        counter++;
                        if (-1 == currentState) {
                            continue;
                        }
                        if (defaultState != currentState) {
                            log.info("Setting value to " + counter + " (within " + (System.currentTimeMillis() - before) + "ms) for "
                                + gpio + (FileHandlingFactory.isSimulation() ? " (SIMULATION)" : ""));
                            break;
                        }
                    }
                    if (tiMaxCounter == counter) {
                        counter = ConfiguredGpio.NO_STATE;
                        log.warn("Max reached (" + tiMaxCounter + "). Check your configuration or hardware: Setting value to "
                            + counter + " (within " + (System.currentTimeMillis() - before) + "ms) for "
                            + gpio + (FileHandlingFactory.isSimulation() ? " (SIMULATION)" : ""));
                    }
                } else {
                    log.error("File " + aGpioFile + " does not exist. Check previous errors.");
                }
            } catch (Exception e) {
                log.error("Got " + e.getClass().getName() + " with message: " + e.getMessage() + " with file " + aGpioFile);
            } finally {
                try {
                    if (null != aGpioRandomAccessFile) {
                        aGpioRandomAccessFile.close();
                    }
                } catch (Exception e) {
                    // catch away
                }
            }
        }

        // 6. The counter of the while loop is returned (which can be checked with conditions).
        returnValue = Long.toString(counter);
        currentOutputPortState.put(gpio, returnValue);
        return returnValue;
        // 7. It stays an input port until next read.
    }


    /**
     * methods really reads from SysFs
     *
     * @param gpio which will be read
     * @return String read value or -1 in case of error
     */

    private String readGpio(ConfiguredGpio gpio) {
        String returnValue = Integer.toString(ConfiguredGpio.NO_STATE);
        FileReader aGpioFileReader = null;
        BufferedReader bf = null;
        File aGpioFile = null;
        synchronized (lockObject) {
            try {
                aGpioFile = FileHandlingFactory.getFile(SYSFS_GPIO_FILE_PREFIX + gpio.getGpioPin().getNumber() + SYSFS_GPIO_VALUE_FILE_SUFFIX);
                if (aGpioFile.exists()) {
                    aGpioFileReader = FileHandlingFactory.getFileReader(aGpioFile);
                    bf = new BufferedReader(aGpioFileReader);
                    returnValue = bf.readLine();
                    log.info("Read " + returnValue + " for " + gpio + (FileHandlingFactory.isSimulation() ? " (SIMULATION)" : ""));
                } else {
                    log.error("File " + aGpioFile + " does not exist. Check previous errors.");
                }
            } catch (Exception e) {
                log.error("Got " + e.getClass().getName() + " with message: " + e.getMessage() + " with file " + aGpioFile);
            } finally {
                try {
                    if (null != bf) {
                        bf.close();
                    }
                } catch (Exception e) {
                    // catch away
                }
                try {
                    if (null != aGpioFileReader) {
                        aGpioFileReader.close();
                    }
                } catch (Exception e) {
                    // catch away
                }
            }
        }
        return returnValue;
    }

    /**
     * method writes to SysFs in an optimized way
     *
     * @param outGpioValueMap      Map must contain gpios (configured as output) and
     *                             value to set
     * @param isAutoTogglingThread must be true if called from
     *                             GpioOutAutoToggleThread
     * @return Map name as key and written value or -1 in case of error
     */
    Map<String, String> writeGpios(Map<ConfiguredGpio, String> outGpioValueMap, boolean isAutoTogglingThread) {
        Map<String, String> returnMap = new HashMap<String, String>();
        // set state of pins
        List<ConfiguredGpio> autoToggleList = new ArrayList<ConfiguredGpio>();
        File aGpioFile = null;
        // keep some arrays for performance reasons
        FileWriter[] perfFileWriterArray = new FileWriter[outGpioValueMap.size()];
        String[] perfValueArray = new String[perfFileWriterArray.length];
        ConfiguredGpio[] perfileConfiguredGpioArray = new ConfiguredGpio[perfFileWriterArray.length];
        int perfIndex = 0;
        Long nowForBlocking = System.currentTimeMillis();

        // for each gpio prepare (if blocked and add to perfArrays)
        for (ConfiguredGpio gpio : outGpioValueMap.keySet()) {
            String value = outGpioValueMap.get(gpio);
            // respect block time if not autoToggling (only on user request)
            // since blocktime > autoToggleTime, a gpio can only be in one GpioOutAutoToggleThread
            if (!isAutoTogglingThread && gpio.getBlockTime() != ConfiguredGpio.NO_DEFAULT_TIME) {
                if (isBlocked(gpio, nowForBlocking)) {
                    returnMap.put(gpio.getUserdefinedName(), Integer.toString(ConfiguredGpio.BLOCKED_STATE));
                    continue;
                }
            }
            aGpioFile = FileHandlingFactory.getFile(SYSFS_GPIO_FILE_PREFIX + gpio.getGpioPin().getNumber() + SYSFS_GPIO_VALUE_FILE_SUFFIX);
            if (aGpioFile.exists()) {
                try {
                    perfFileWriterArray[perfIndex] = FileHandlingFactory.getFileWriter(aGpioFile);
                    perfValueArray[perfIndex] = value + String.format("%n");
                    perfileConfiguredGpioArray[perfIndex] = gpio;
                    perfIndex++;
                    log.info("Will set state " + value + (isAutoTogglingThread ? " (autoToggle)" : "") + " for " + gpio);
                    returnMap.put(gpio.getUserdefinedName(), value);
                    // if not running in autoToggling, check if configured and keep a list
                    if (!isAutoTogglingThread && gpio.getAutoToogleTime() != ConfiguredGpio.NO_DEFAULT_TIME) {
                        autoToggleList.add(gpio);
                    }
                } catch (Exception e) {
                    FileWriter aGpioFileWriter = perfFileWriterArray[perfIndex];
                    perfFileWriterArray[perfIndex] = null;
                    try {
                        if (null != aGpioFileWriter) {
                            aGpioFileWriter.close();
                        }
                    } catch (Exception ef) {
                        // catch away
                    }
                    perfIndex++; // increment also in error case, since it is decremented later
                }
            } else {
                log.error("File " + aGpioFile + " does not exists. Check previous errors.");
                returnMap.put(gpio.getUserdefinedName(), Integer.toString(ConfiguredGpio.NO_STATE));
            }
        }

        int i = 0; // i may be needed in catch block
        synchronized (lockObject) {
            try {
                long before = System.currentTimeMillis();
                for (i = 0; i < perfIndex; i++) {
                    FileWriter aGpioFileWriter = perfFileWriterArray[i];
                    if (null != aGpioFileWriter) {
                        aGpioFileWriter.write(perfValueArray[i]);
                        aGpioFileWriter.flush();
                    } else {
                        log.debug("File handle for file " + aGpioFile + " is null, ignoring.");
                    }
                }
                long after = System.currentTimeMillis();
                log.info("Writing of gpio ports took " + (after - before) + "ms at " + after + (FileHandlingFactory.isSimulation() ? " (SIMULATION)" : ""));
            } catch (Exception e) {
                // if we get here, forget about timing: log and overwrite
                log.error("Got " + e.getClass().getName() + " with message: " + e.getMessage() + " with file " + aGpioFile);
                returnMap.put(perfileConfiguredGpioArray[i].getUserdefinedName(), Integer.toString(ConfiguredGpio.NO_STATE));
            } finally {
                for (i = 0; i < perfIndex; i++) {
                    FileWriter aGpioFileWriter = perfFileWriterArray[i];
                    try {
                        if (null != aGpioFileWriter) {
                            aGpioFileWriter.close();
                            currentOutputPortState.put(perfileConfiguredGpioArray[i], perfValueArray[i].trim());
                        }
                    } catch (Exception e) {
                        log.error("Got a " + e.getClass().getName() + " with message: " + e.getMessage() + " with file " + aGpioFile
                            + " while closing. You may ignore this error, but the cause of upcoming errors may be here.");
                    } finally {
                        aGpioFileWriter = null;
                    }
                }
            }
        }
        // if we have something to toggle back to default state later, do it
        if (!isAutoTogglingThread && !autoToggleList.isEmpty()) {
            GpioOutAutoToggleThread atThread = new GpioOutAutoToggleThread(autoToggleList, (System.currentTimeMillis() - nowForBlocking));
            atThread.start();
        }
        return returnMap;
    }


    /**
     * method checks if a gpio port is blocked. if blocking is configured must be checked before!
     *
     * @param gpio           ConfiguredGpio to check
     * @param nowForBlocking Long millis
     * @return true if blocked
     */
    private boolean isBlocked(ConfiguredGpio gpio, Long nowForBlocking) {
        boolean isBlocked = false;
        Long blockedTimestamp = blockedMap.get(gpio);
        if (null == blockedTimestamp) {
            blockedMap.put(gpio, nowForBlocking);
        } else {
            if ((blockedTimestamp + gpio.getBlockTime()) > nowForBlocking) {
                log.warn("Gpio is currently blocked (another " + ((blockedTimestamp + gpio.getBlockTime()) - nowForBlocking) + " ms) for " + gpio);
                isBlocked = true;
            } else {
                blockedMap.put(gpio, nowForBlocking);
            }
        }
        return isBlocked;
    }
}
