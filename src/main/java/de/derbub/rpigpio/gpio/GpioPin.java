/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio;

/**
 * Class represents a hardware pin (actual just the number is important and has
 * helpers to read in configuration.
 *
 * @author sb
 */
public enum GpioPin {

    GPIO0(0, "GPIO.0"), GPIO1(1, "GPIO.1"), GPIO4(4, "GPIO.4"), GPIO7(7, "GPIO.7"), GPIO8(8, "GPIO.8"),
    GPIO9(9, "GPIO.9"), GPIO10(10, "GPIO.10"), GPIO11(11, "GPIO.11"), GPIO14(14, "GPIO.14"), GPIO15(15, "GPIO.15"),
    GPIO17(17, "GPIO.17"), GPIO18(18, "GPIO.18"), GPIO21(21, "GPIO.21") /* 21 only on rev1 boards */,
    GPIO22(22, "GPIO.22"), GPIO23(23, "GPIO.23"), GPIO24(24, "GPIO.24"), GPIO25(25, "GPIO.25"),
    GPIO27(27, "GPIO.27") /* 27 only on rev2 boards */;
    private int number;
    private String configPrefix;

    GpioPin(int number, String configPrefix) {
        this.number = number;
        this.configPrefix = configPrefix;
    }

    public int getNumber() {
        return number;
    }

    public String getNumberAsStringLine() {
        return Integer.toString(number) + String.format("%n");
    }

    public String getConfigDirectionKey() {
        return configPrefix + ".DIRECTION";
    }

    public String getConfigNameKey() {
        return configPrefix + ".NAME";
    }

    public String getConfigDefaultStateKey() {
        return configPrefix + ".OUT.DEFAULT.STATE";
    }

    public String getConfigAutoToggleTimeKey() {
        return configPrefix + ".OUT.AUTO.TOGGLE.TIME";
    }

    public String getConfigBlockTimeKey() {
        return configPrefix + ".OUT.BLOCK.TIME";
    }

    @Override
    public String toString() {
        return configPrefix;
    }
}
