/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio.files;

import de.derbub.rpigpio.gpio.ConfiguredGpio;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.CharBuffer;

/**
 * a FileReader mock for simulation
 *
 * @author sb
 */
public class FileReaderSimulation extends FileReader {

    private static Logger log = Logger.getLogger(FileHandlingFactory.class);
    boolean alreadyReadOnce = false; // this mock only allows reading once

    public FileReaderSimulation(FileDescriptor fd) {
        super(fd);
        throw new RuntimeException("Not yet implemented");
    }

    public FileReaderSimulation(File file) throws FileNotFoundException {
        super(new FileSimulation("/bin/sh"));
    }

    public FileReaderSimulation(String string) throws FileNotFoundException {
        super("/bin/sh");
    }

    @Override
    public void close() throws IOException {
        super.close();
    }

    @Override
    public String getEncoding() {
        return super.getEncoding();
    }

    @Override
    public int read() throws IOException {
        return 97;
    }

    @Override
    public int read(char[] chars, int offset, int length) throws IOException {
        int retVal = -1;
        if (!alreadyReadOnce) {
            alreadyReadOnce = true;
            retVal = 1;
            char[] noStateChar = Integer.toString(2).toCharArray();
            System.arraycopy(noStateChar, 0, chars, offset, noStateChar.length);
        }
        return retVal;
    }

    @Override
    public boolean ready() throws IOException {
        return true;
    }

    @Override
    public void mark(int i) throws IOException {
    }

    @Override
    public boolean markSupported() {
        return false;
    }

    @Override
    public int read(CharBuffer cb) throws IOException {
        log.error("read(CharBuffer cb): NOT YET IMPLEMENTED");
        return -1;
    }

    @Override
    public int read(char[] chars) throws IOException {
        log.error("read(char[] chars):  NOT YET IMPLEMENTED");
        return -1;
    }

    @Override
    public void reset() throws IOException {
    }

    @Override
    public long skip(long l) throws IOException {
        return 0;
    }
}
