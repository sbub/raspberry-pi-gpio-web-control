/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * a RandomAccessFile mock for simulation
 * <p/>
 * minimum is implemented to simulate TI direction
 *
 * @author sb
 */
public class RandomAccessFileSimulation extends RandomAccessFile {

    public RandomAccessFileSimulation(String name, String mode) throws FileNotFoundException {
        super(name, mode);
        throw new RuntimeException("Not yet implemented");
    }

    public RandomAccessFileSimulation(File file, String mode) throws FileNotFoundException {
        super(new FileSimulation("/bin/sh"), "r");
    }

    @Override
    public int read() throws IOException {
        return 49;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public int read(byte[] b) throws IOException {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public int skipBytes(int n) throws IOException {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void write(int b) throws IOException {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void write(byte[] b) throws IOException {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public long getFilePointer() throws IOException {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void seek(long pos) throws IOException {
        // do nothing on purpose
    }

    @Override
    public long length() throws IOException {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void setLength(long newLength) throws IOException {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void close() throws IOException {
        super.close();
    }
}
