/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio.files;

import java.io.*;

/**
 * a FileWriter mock for simulation
 *
 * @author sb
 */
public class FileWriterSimulation extends FileWriter {

    public FileWriterSimulation(FileDescriptor fd) {
        super(fd);
        throw new RuntimeException("Not yet implemented");
    }

    public FileWriterSimulation(File file, boolean bln) throws IOException {
        super(new FileSimulation("/tmp/FileWriterSimulation"), bln);
    }

    public FileWriterSimulation(File file) throws IOException {
        super(new FileSimulation("/tmp/FileWriterSimulation"));
    }

    public FileWriterSimulation(String string, boolean bln) throws IOException {
        super("/tmp/FileWriterSimulation", bln);
    }

    public FileWriterSimulation(String string) throws IOException {
        super("/tmp/FileWriterSimulation");
    }

    @Override
    public void close() throws IOException {
        super.close();
    }

    @Override
    public void flush() throws IOException {
    }

    @Override
    public String getEncoding() {
        return super.getEncoding();
    }

    @Override
    public void write(int i) throws IOException {
    }

    @Override
    public void write(char[] chars, int i, int i1) throws IOException {
    }

    @Override
    public void write(String string, int i, int i1) throws IOException {
    }

    @Override
    public Writer append(CharSequence cs) throws IOException {
        return this;
    }

    @Override
    public Writer append(CharSequence cs, int i, int i1) throws IOException {
        return this;
    }

    @Override
    public Writer append(char c) throws IOException {
        return this;
    }

    @Override
    public void write(char[] chars) throws IOException {
    }

    @Override
    public void write(String string) throws IOException {
    }

}
