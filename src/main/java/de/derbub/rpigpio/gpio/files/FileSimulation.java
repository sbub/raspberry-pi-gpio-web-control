/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio.files;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * a File mock for simulation
 * <p/>
 * it acts as if a lot is possible
 *
 * @author sb
 */
public class FileSimulation extends File {

    public FileSimulation(File parent, String child) {
        super(parent, child);
    }

    public FileSimulation(String pathname) {
        super(pathname);
    }

    public FileSimulation(String parent, String child) {
        super(parent, child);
    }

    public FileSimulation(URI uri) {
        super(uri);
    }

    @Override
    public boolean canExecute() {
        return true;
    }

    @Override
    public boolean canRead() {
        return true;
    }

    @Override
    public boolean canWrite() {
        return true;
    }

    @Override
    public int compareTo(File file) {
        return super.compareTo(file);
    }

    @Override
    public boolean createNewFile() throws IOException {
        return true;
    }

    @Override
    public boolean delete() {
        return true;
    }

    @Override
    public void deleteOnExit() {
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public boolean exists() {
        return true;
    }

    @Override
    public File getAbsoluteFile() {
        return new FileSimulation(this.getAbsolutePath());
    }

    @Override
    public String getAbsolutePath() {
        return super.getAbsolutePath();
    }

    @Override
    public File getCanonicalFile() throws IOException {
        return new FileSimulation(this.getCanonicalPath());
    }

    @Override
    public String getCanonicalPath() throws IOException {
        return super.getCanonicalPath();
    }

    @Override
    public long getFreeSpace() {
        return 1000L;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getParent() {
        return super.getParent();
    }

    @Override
    public File getParentFile() {
        String s = getParent();
        if (s != null) {
            return new FileSimulation(s);
        } else {
            return null;
        }
    }

    @Override
    public String getPath() {
        return super.getPath();
    }

    @Override
    public long getTotalSpace() {
        return 1000L;
    }

    @Override
    public long getUsableSpace() {
        return super.getUsableSpace();
    }

    @Override
    public int hashCode() {
        return super.hashCode() + 3; // be different than real File 
    }

    @Override
    public boolean isAbsolute() {
        return super.isAbsolute();
    }

    @Override
    public boolean isDirectory() {
        return true;
    }

    @Override
    public boolean isFile() {
        return true;
    }

    @Override
    public boolean isHidden() {
        return false;
    }

    @Override
    public long lastModified() {
        return System.currentTimeMillis();
    }

    @Override
    public long length() {
        return 1000L;
    }

    @Override
    public String[] list() {
        return super.list();
    }

    @Override
    public String[] list(FilenameFilter ff) {
        return super.list(ff);
    }

    @Override
    public File[] listFiles() {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public File[] listFiles(FilenameFilter ff) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public File[] listFiles(FileFilter ff) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public boolean mkdir() {
        return true;
    }

    @Override
    public boolean mkdirs() {
        return true;
    }

    @Override
    public boolean renameTo(File file) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public boolean setExecutable(boolean bln, boolean bln1) {
        return true;
    }

    @Override
    public boolean setExecutable(boolean bln) {
        return true;
    }

    @Override
    public boolean setLastModified(long l) {
        return true;
    }

    @Override
    public boolean setReadOnly() {
        return true;
    }

    @Override
    public boolean setReadable(boolean bln, boolean bln1) {
        return true;
    }

    @Override
    public boolean setReadable(boolean bln) {
        return true;
    }

    @Override
    public boolean setWritable(boolean bln, boolean bln1) {
        return true;
    }

    @Override
    public boolean setWritable(boolean bln) {
        return true;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public URI toURI() {
        return super.toURI();
    }

    @Override
    public URL toURL() throws MalformedURLException {
        return super.toURL();
    }
}
