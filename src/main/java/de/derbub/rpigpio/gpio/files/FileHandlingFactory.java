/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio.files;

import org.apache.log4j.Logger;

import java.io.*;

/**
 * Simple factory which may return mock objects in case of simulation mode
 *
 * @author sb
 */
public class FileHandlingFactory {

    private static Logger log = Logger.getLogger(FileHandlingFactory.class);
    private static Boolean SIMULATE_GPIOS = Boolean.TRUE;

    /**
     * method set the state if simulation should be used
     *
     * @param b boolean
     */
    public static void setSimulateGpios(Boolean b) {
        SIMULATE_GPIOS = b;
    }

    public static boolean isSimulation() {
        return SIMULATE_GPIOS;
    }

    public static File getFile(String s) {
        if (SIMULATE_GPIOS) {
            log.info("Return FileSimulation for " + s);
            return new FileSimulation(s);
        } else {
            log.debug("Return File for " + s);
            return new File(s);
        }
    }

    public static FileWriter getFileWriter(File f) throws IOException {
        if (SIMULATE_GPIOS) {
            log.info("Return FileWriterSimulation for " + f);
            return new FileWriterSimulation(f);
        } else {
            log.debug("Return FileWriter for " + f);
            return new FileWriter(f);
        }
    }

    public static FileReader getFileReader(File f) throws FileNotFoundException {
        if (SIMULATE_GPIOS) {
            log.info("Return FileReaderSimulation for " + f);
            return new FileReaderSimulation(f);
        } else {
            log.debug("Return FileReader for " + f);
            return new FileReader(f);
        }
    }

    public static RandomAccessFile getRandomAccessFile(File f, String mode) throws FileNotFoundException {
        if (SIMULATE_GPIOS) {
            log.info("Return FileReaderSimulation for " + f);
            return new RandomAccessFileSimulation(f, mode);
        } else {
            log.debug("Return FileReader for " + f);
            return new RandomAccessFile(f, mode);
        }
    }

}
