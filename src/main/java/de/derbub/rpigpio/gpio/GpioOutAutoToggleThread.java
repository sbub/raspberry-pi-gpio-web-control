/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class implements a thread which will set given gpios to default state after auto
 * toogle time. This class is initialized each time a request is done (or cronjob is
 * running) if a port has an autotoggle time (there might me exist multiple instances of this
 * class at once).
 *
 * @author sb
 */
public class GpioOutAutoToggleThread extends Thread {

    private static Logger log = Logger.getLogger(GpioOutAutoToggleThread.class);
    List<ConfiguredGpio> gpioList;
    Long sysfsProcessingTime;

    public GpioOutAutoToggleThread(List<ConfiguredGpio> gpioList, Long sysfsProcessingTime) {
        this.gpioList = gpioList;
        this.sysfsProcessingTime = sysfsProcessingTime;
    }

    @Override
    public void run() {
        log.debug("run()");
        long absSleepTime = 0L;
        long currentDeltaSleepTime;

        while (!gpioList.isEmpty()) {
            // find smallest auto toggle time
            long tmpSleepTime = 0L;
            for (ConfiguredGpio gpio : gpioList) {
                if (tmpSleepTime == 0L) {
                    tmpSleepTime = gpio.getAutoToogleTime();
                }
                if (tmpSleepTime > gpio.getAutoToogleTime()) {
                    tmpSleepTime = gpio.getAutoToogleTime();
                }
            }
            // set new delta time
            currentDeltaSleepTime = tmpSleepTime - absSleepTime;
            absSleepTime = tmpSleepTime;
            try {
                currentDeltaSleepTime -= sysfsProcessingTime; // remove some time for processing here
                if (currentDeltaSleepTime > 0) {
                    log.debug("run() sleep for " + currentDeltaSleepTime + " ms");
                    sleep(currentDeltaSleepTime);
                } else {
                    log.debug("run() not sleeping already late by " + currentDeltaSleepTime + " ms");
                }
            } catch (InterruptedException e) {
                log.error("run() interrupted (shutdown in progress?). Thread is stopped!");
                break;
            }
            long before = System.currentTimeMillis();
            // after delta time, find notable
            Map<ConfiguredGpio, String> outGpioValueMap = new HashMap<ConfiguredGpio, String>();
            for (ConfiguredGpio gpio : gpioList) {
                if (gpio.getAutoToogleTime() == absSleepTime) {
                    outGpioValueMap.put(gpio, Integer.toString(gpio.getDefaultState()));
                }
            }
            // remove notable from list and handle them
            gpioList.removeAll(outGpioValueMap.keySet());
            GpioSysFsInterface.getInitializedInstance().writeGpios(outGpioValueMap, true);
            sysfsProcessingTime = System.currentTimeMillis() - before;
        }
        log.debug("run() finished");
    }
}
