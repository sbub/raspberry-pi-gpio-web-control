/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.gpio;

import de.derbub.rpigpio.gpio.files.FileHandlingFactory;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class is used internally for initialization of gpio ports using SysFs
 *
 * @author sb
 */
class GpioSysFsInitializer {

    private static Logger log = Logger.getLogger(GpioSysFsInitializer.class);
    private static final String SYSFS_GPIO_EXPORT_FILE = "/sys/class/gpio/export";
    private static final String SYSFS_GPIO_UNEXPORT_FILE = "/sys/class/gpio/unexport";
    private static final String SYSFS_GPIO_DIRECTION_FILE_SUFFIX = "/direction";
    static final String SYSFS_GPIO_FILE_PREFIX = "/sys/class/gpio/gpio";
    static final String SYSFS_GPIO_VALUE_FILE_SUFFIX = "/value";
    static Map<String, ConfiguredGpio> configuredGpioMap = new HashMap<String, ConfiguredGpio>();
    static Map<ConfiguredGpio, String> currentOutputPortState = new HashMap<ConfiguredGpio, String>();
    final Object lockObject = new Object();

    /**
     * return the internal map which must not be changed!
     *
     * @return Map
     */
    public Map<String, ConfiguredGpio> getConfiguredGpioMap() {
        return configuredGpioMap;
    }

    /**
     * methods exports gpio ports and sets its direction
     *
     * @return boolean success
     */
    public boolean setupAllGpioPorts() {
        FileWriter exportFileWriter = null;
        // export gpio pins
        synchronized (lockObject) {
            try {
                File expFile = FileHandlingFactory.getFile(SYSFS_GPIO_EXPORT_FILE);
                if (expFile.exists()) {
                    exportFileWriter = FileHandlingFactory.getFileWriter(expFile);
                    for (ConfiguredGpio gpio : configuredGpioMap.values()) {
                        exportFileWriter.write(gpio.getGpioPin().getNumberAsStringLine());
                        exportFileWriter.flush();
                        log.info("Export of " + gpio);
                    }
                } else {
                    log.error("File " + SYSFS_GPIO_EXPORT_FILE + " does not exists. Check your OS or hardware.");
                    return false;
                }
            } catch (IOException e) {
                log.error("Got " + e.getClass().getName() + " with message: " + e.getMessage() + " with file " + SYSFS_GPIO_EXPORT_FILE);
                return false;
            } finally {
                try {
                    if (null != exportFileWriter) {
                        exportFileWriter.close();
                    }
                } catch (Exception e) {
                    // catch away
                }
            }
        }

        // set direction of pins
        for (ConfiguredGpio gpio : configuredGpioMap.values()) {
            Direction d;
            if (Direction.TI.equals(gpio.getDirection())) {
                d = Direction.IN;
            } else {
                d = gpio.getDirection();
            }
            if (!setDirection(gpio, d)) {
                return false;
            }
        }
        return true;
    }

    /**
     * mehtod sets the direction of a gpio
     *
     * @param gpio      ConfiguredGpio to set direction
     * @param direction Direction to set (should be read from same gpio before (if TI direction depends on the state)
     * @return true if success, else false
     */
    boolean setDirection(ConfiguredGpio gpio, Direction direction) {
        FileWriter aGpioFileWriter = null;
        File aGpioFile = null;
        synchronized (lockObject) {
            try {
                aGpioFile = FileHandlingFactory.getFile(SYSFS_GPIO_FILE_PREFIX + gpio.getGpioPin().getNumber() + SYSFS_GPIO_DIRECTION_FILE_SUFFIX);
                if (aGpioFile.exists()) {
                    aGpioFileWriter = FileHandlingFactory.getFileWriter(aGpioFile);
                    aGpioFileWriter.write(direction.getDirectionStringLine());
                    aGpioFileWriter.flush();
                    log.info("Set direction for " + gpio + " to " + direction);
                } else {
                    log.error("File " + aGpioFile + " does not exists. Check previous errors.");
                    return false;
                }
            } catch (Exception e) {
                log.error("Got " + e.getClass().getName() + " with message: " + e.getMessage() + " with file " + aGpioFile);
                return false;
            } finally {
                try {
                    if (null != aGpioFileWriter) {
                        aGpioFileWriter.close();
                    }
                } catch (Exception e) {
                    // catch away
                } finally {
                    aGpioFileWriter = null;
                }
            }
        }
        return true;
    }

    /**
     * methods sets the default state of each configured gpio port
     *
     * @return boolean success
     */
    public boolean setAllDefaultState() {
        // set default state of pins
        FileWriter aGpioFileWriter = null;
        File aGpioFile = null;
        synchronized (lockObject) {
            for (ConfiguredGpio gpio : configuredGpioMap.values()) {
                try {
                    currentOutputPortState.put(gpio, Integer.toString(gpio.getDefaultState()));
                    if (!Direction.OUT.equals(gpio.getDirection()) || gpio.getDefaultState() == ConfiguredGpio.NO_STATE) {
                        log.debug("Set no default state for " + gpio);
                        continue;
                    }
                    aGpioFile = FileHandlingFactory.getFile(SYSFS_GPIO_FILE_PREFIX + gpio.getGpioPin().getNumber() + SYSFS_GPIO_VALUE_FILE_SUFFIX);
                    if (aGpioFile.exists()) {
                        aGpioFileWriter = FileHandlingFactory.getFileWriter(aGpioFile);
                        aGpioFileWriter.write(gpio.getDefaultStateAsStringLine());
                        aGpioFileWriter.flush();
                        log.info("Set default state for " + gpio);
                    } else {
                        log.error("File " + aGpioFile + " does not exists. Check previous errors.");
                        return false;
                    }
                } catch (Exception e) {
                    log.error("Got " + e.getClass().getName() + " with message: " + e.getMessage() + " with file " + aGpioFile);
                    return false;
                } finally {
                    try {
                        if (null != aGpioFileWriter) {
                            aGpioFileWriter.close();
                        }
                    } catch (Exception e) {
                        // catch away
                    } finally {
                        aGpioFileWriter = null;
                    }
                }
            }
        }
        return true;
    }

    /**
     * methods sets all output pins to 0
     *
     * @return boolean success
     */
    boolean resetAllState() {
        // set state 0 for all pins
        FileWriter aGpioFileWriter = null;
        File aGpioFile = null;
        boolean isSuccessful = true;
        synchronized (lockObject) {
            for (ConfiguredGpio gpio : configuredGpioMap.values()) {
                if (!gpio.getDirection().isOutput()) {
                    continue;
                }
                try {
                    aGpioFile = FileHandlingFactory.getFile(SYSFS_GPIO_FILE_PREFIX + gpio.getGpioPin().getNumber() + SYSFS_GPIO_VALUE_FILE_SUFFIX);
                    if (aGpioFile.exists()) {
                        aGpioFileWriter = FileHandlingFactory.getFileWriter(aGpioFile);
                        aGpioFileWriter.write("0" + String.format("%n"));
                        aGpioFileWriter.flush();
                        log.info("Set state 0 for " + gpio);
                    } else {
                        log.error("File " + aGpioFile + " does not exists. Check previous errors.");
                        isSuccessful = false;
                    }
                } catch (Exception e) {
                    log.error("Got " + e.getClass().getName() + " with message: " + e.getMessage() + " with file " + aGpioFile);
                    isSuccessful = false;
                } finally {
                    try {
                        if (null != aGpioFileWriter) {
                            aGpioFileWriter.close();
                        }
                    } catch (Exception e) {
                        // catch away
                    } finally {
                        aGpioFileWriter = null;
                    }
                }
            }
        }
        return isSuccessful;
    }

    /**
     * method unexports all configured gpio pins
     *
     * @return boolean success
     */
    boolean unExportAllGpioPorts() {
        FileWriter exportFileWriter = null;
        boolean isSuccessful = true;
        // unexport all gpio pins
        synchronized (lockObject) {
            for (ConfiguredGpio gpio : configuredGpioMap.values()) {
                try {
                    File unexpFile = FileHandlingFactory.getFile(SYSFS_GPIO_UNEXPORT_FILE);
                    if (unexpFile.exists()) {
                        exportFileWriter = FileHandlingFactory.getFileWriter(unexpFile);
                        exportFileWriter.write(gpio.getGpioPin().getNumberAsStringLine());
                        exportFileWriter.flush();
                        log.info("Unexport of " + gpio);
                    } else {
                        log.error("File " + SYSFS_GPIO_UNEXPORT_FILE + " does not exists. Check your OS or hardware.");
                        isSuccessful = false;
                    }
                } catch (IOException e) {
                    log.error("Got " + e.getClass().getName() + " with message: " + e.getMessage() + " with file " + SYSFS_GPIO_UNEXPORT_FILE);
                    isSuccessful = false;
                } finally {
                    try {
                        if (null != exportFileWriter) {
                            exportFileWriter.close();
                        }
                    } catch (Exception e) {
                        // catch away
                    }
                }
            }
        }
        return isSuccessful;
    }
}
