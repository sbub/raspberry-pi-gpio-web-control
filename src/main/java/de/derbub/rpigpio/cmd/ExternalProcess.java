/*
 *
 * Copyright 2013 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.cmd;

import org.apache.log4j.Logger;

import java.io.*;

/**
 * Represents an external process as defined as a command
 */
public class ExternalProcess {

    private static Logger log = Logger.getLogger(ExternalProcess.class);

    String commandName;
    String externalCommand;
    private long commandTimeout;

    /**
     * constructor
     *
     * @param commandName     name defined in config
     * @param externalCommand command as string
     * @param commandTimeout  timeout in ms
     * @throws IOException in case that the script has a problem
     */
    public ExternalProcess(String commandName, String externalCommand, long commandTimeout) throws IOException {
        this.commandName = commandName;
        this.externalCommand = externalCommand;
        this.commandTimeout = commandTimeout;
        checkExternalCommandString(externalCommand);
    }

    /**
     * this will start the command and pipe the parameter to it with STDIN
     *
     * @param parameter the parameter that is given to external process
     * @return the return code as string from the external process or -1 if a timeout happened
     */
    public String start(String parameter) {
        String exitVal = "-1";

        try {
            log.debug("starting external process: " + externalCommand + " with timeout " + commandTimeout);
            Process proc = (null == parameter)
                ? new ProcessBuilder(externalCommand).start()
                : new ProcessBuilder(externalCommand, parameter).start();
            // setup IO
            BufferedReader stderr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
            BufferedReader stdout = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            // timeout thread
            TimeoutRunner runner = new TimeoutRunner(proc, commandName, commandTimeout);

            try {
                exitVal = Integer.toString(proc.waitFor());
                log.info("external process exited with code: " + exitVal + " with parameter " + parameter);
            } catch (InterruptedException e) {
                log.warn("external process" + externalCommand + "was interrupted and probably has not finished on parameter " + parameter);
            }
            if (runner.isThreadAlive()) {
                runner.interruptThread();
            }

            try {
                // reading IO
                String val;
                while ((val = stdout.readLine()) != null) {
                    log.debug("        stdout: " + val);
                }
                while ((val = stderr.readLine()) != null) {
                    log.debug("        stderr: " + val);
                }
            } catch (IOException e) {
                /* ignore it */
            }
        } catch (IOException e) {
            log.error("Unhandled IOException : " + e, e);
        } catch (Throwable e) {
            log.error("Unexpected and unhandled exception: " + e, e);
        }
        return exitVal;
    }


    /**
     * Method checks validity of external command
     *
     * @throws IOException is thrown if script does not exists, is not a file
     *                     or is not executable
     */
    private void checkExternalCommandString(String cmd) throws IOException {
        File file = new File(cmd);
        if (file.exists() && file.isFile() && file.canExecute()) {
            // everything is fine, do nothing
        } else {
            String msg = "cmd file " + cmd + " not valid: "
                + (file.exists() ? "" : " [does not exists] ")
                + (file.isFile() ? "" : " [is not a file] ")
                + (file.canExecute() ? "" : " [is not executable] ");
            throw new IOException(msg);
        }
    }

}
