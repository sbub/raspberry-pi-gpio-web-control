/*
 *
 * Copyright 2013 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.cmd;


import org.apache.log4j.Logger;

/**
 * starts a thread for a process to watch after it
 */
public class TimeoutRunner implements Runnable {

    private static Logger log = Logger.getLogger(TimeoutRunner.class);

    private Process runnableProcess;
    private long runnableTimeout;
    private Thread timeoutThread;
    private String timeoutThreadName;

    /** creates a thread that will interrupt the runnableProcess after runnableTimeout
     *  if it does not get interrupted by interruptThread()
     *
     * @param runnableProcess the already started process
     * @param name the name of it
     * @param runnableTimeout timeout in ms
     */
    public TimeoutRunner(Process runnableProcess, String name, long runnableTimeout) {
        super();
        this.runnableProcess = runnableProcess;
        this.runnableTimeout = runnableTimeout;

        timeoutThread = new Thread(this);
        timeoutThreadName = "TimeoutRunner-" + name;
        timeoutThread.setName(timeoutThreadName);
        timeoutThread.start();
    }

    /**
     *
     * @return true if thread is still alive
     */
    public boolean isThreadAlive() {
        return timeoutThread.isAlive();
    }

    /**
     *  interrupts the thread
     */
    public void interruptThread() {
        log.debug("interrupting " + timeoutThreadName);
        timeoutThread.interrupt();
    }

    /**
     * run method
     */
    public void run() {
        log.debug("[processWatcherThread]: new thread started with timeout " + runnableTimeout + "ms");
        try {
            Thread.sleep(runnableTimeout);
            log.warn("[processWatcherThread]: timeout run ended - will destroy external process now");
            runnableProcess.destroy();
        } catch (InterruptedException e) {
            log.debug("[processWatcherThread]: timeout run was interrupted");
        }
    }

}
