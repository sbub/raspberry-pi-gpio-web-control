/*
 *
 * Copyright 2013 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.cmd;


import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * read in configuration and holds a map of all external processes
 */
public class ExternalProcessesManager {

    private static Logger log = Logger.getLogger(ExternalProcessesManager.class);

    private static Map<String, ExternalProcess> externalProcesses = new HashMap<String, ExternalProcess>();
    public static final String CMD_KEY_PREFIX = "CMD";

    public static final String NO_OF_EXTCMD = "EXTCMD.NO";
    public static final String EXTCMD_NAME_KEY_PREFIX = "EXTCMD.NAME.";
    public static final String EXTCMD_COMMANDNAME_KEY_PREFIX = "EXTCMD.COMMANDNAME.";
    public static final String EXTCMD_TIMEOUT_KEY_PREFIX = "EXTCMD.TIMEOUT.";

    /**
     * constuctor reads in config file and initialized ExternalProcess Map
     *
     * @param cmdConfigFile a File which referenceres the properties
     */
    public ExternalProcessesManager(File cmdConfigFile) {
        Properties configProperties = new Properties();

        if (!cmdConfigFile.exists()) {
            log.info("Configuration for external processes not found");
            return;
        }

        log.debug("reading file " + cmdConfigFile);

        // read configuration file to configProperties
        try {
            configProperties.load(new FileReader(cmdConfigFile));
        } catch (IOException e) {
            log.error(e.getClass().getName() + ":" + e.getMessage());
            return;
        }

        String noOfExtCmdString = configProperties.getProperty(NO_OF_EXTCMD);
        int noOfExtCmd;
        try {
            noOfExtCmd = Integer.parseInt(noOfExtCmdString);
            log.debug(NO_OF_EXTCMD + " is set to " + noOfExtCmd);
        } catch (NumberFormatException e) {
            log.error(NO_OF_EXTCMD + " is not a number. NOT configuring any external command.");
            return;
        }

        for (int i = 1; i <= noOfExtCmd; i++) {
            ExternalProcess ep;
            String name = configProperties.getProperty(EXTCMD_NAME_KEY_PREFIX + i);
            if (name == null || name.trim().equals("")) {
                log.warn(EXTCMD_NAME_KEY_PREFIX + i +" not set. Ignoring command.");
                continue;
            }

            String commandName = configProperties.getProperty(EXTCMD_COMMANDNAME_KEY_PREFIX + i);
            if (commandName == null || commandName.trim().equals("")) {
                log.warn(EXTCMD_COMMANDNAME_KEY_PREFIX + i +" not set. Ignoring command.");
                continue;
            }

            try {
                long timeout;
                try {
                    timeout = Long.parseLong(configProperties.getProperty(EXTCMD_TIMEOUT_KEY_PREFIX + i));
                } catch (NumberFormatException nfe){
                    log.warn(EXTCMD_TIMEOUT_KEY_PREFIX + i +" not a number. Ignoring command.");
                    continue;
                }
                if (timeout <= 0) {
                    log.warn(EXTCMD_TIMEOUT_KEY_PREFIX + i +" is less or equal 0. Ignoring command.");
                    continue;
                }
                ep = new ExternalProcess(name, commandName, timeout);
                ExternalProcess previousP = externalProcesses.put(CMD_KEY_PREFIX + name, ep);
                if (null != previousP) {
                    log.warn("Multiple commands named: " + previousP.commandName + ". Ignoring old " + previousP.externalCommand);
                } else {
                    log.info("Commands named: " + name + " configured with " + commandName + " and timeout " + timeout + "ms");
                }
            } catch (IOException e) {
                log.warn(EXTCMD_NAME_KEY_PREFIX + i +" throws exception during init (ignoring command): " + e.getMessage());
            }
        }
    }

    /**
     * calls the command with the given parameter
     *
     * @param name the name prefixed with CMD
     * @param parameter the parameter given to the external process
     * @return the exit code of the external process
     */
    public String process(String name, String parameter) {
        ExternalProcess ep = externalProcesses.get(name);
        if (null == ep) {
            log.warn("Did not find process with name " + name + " (not found) - will return -1");
            return "-1";
        }
        return ep.start(parameter);
    }

}
