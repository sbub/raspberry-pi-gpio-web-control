/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.web;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

/*
   This class is a static example with a minimum number of lines of code.
   It does not belong to the real project and only exists to be an example.

   To make this static sample work you would need to replace the servlet and servlet mapping
   in the file web.xml after the line that starts with <display-name> with the following lines.

    <servlet>
        <servlet-name>StaticSampleServlet</servlet-name>
        <servlet-class>de.derbub.rpigpio.web.StaticSampleServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>StaticSampleServlet</servlet-name>
        <url-pattern>/staticsample</url-pattern>
    </servlet-mapping>

 */
public class StaticSampleServlet
    extends HttpServlet {

    public void init() {
        writeFile("export", "0");
        writeFile("gpio0/direction", "out");
        writeFile("gpio0/value", "0");
    }

    public void destroy() {
        writeFile("unexport", "0");
    }

    public void doGet(HttpServletRequest req,
                      HttpServletResponse resp)
        throws ServletException, IOException {
        writeFile("gpio0/value", "1");
        new Timer().schedule(new TimerTask() {
            public void run() {
                writeFile("gpio0/value", "0");
            }
        }, 900);
    }

    private void writeFile(String fileNameString, String value) {
        FileWriter fw = null;
        try {
            fw = new FileWriter("/sys/class/gpio/" + fileNameString);
            fw.write(value);
        } catch (IOException e) {
            e.printStackTrace(); /* and handle adequately */
        } finally {
            try {
                fw.close();
            } catch (IOException ee) {
                ee.printStackTrace(); /* and handle adequately */
            }
        }
    }
}
