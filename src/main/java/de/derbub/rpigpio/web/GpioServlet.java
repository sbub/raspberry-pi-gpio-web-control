/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.web;

import de.derbub.rpigpio.SimpleCommand;
import de.derbub.rpigpio.SingleContext;
import de.derbub.rpigpio.cron.CronjobAdapter;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Main front component servlet which reads configuration file during init(),
 * clears pins during destroy() and handles GET and POST requests.
 *
 * @author sb
 */
public class GpioServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(GpioServlet.class);
    private static CronjobAdapter cron;
    private static SingleContext context;
    private static final String SYSTEM_PROPERTY_CRON_CONFIG = "cron.configuration";
    private static final String SYSTEM_PROPERTY_GPIO_CONFIG = "gpio.configuration";
    private static final String SYSTEM_PROPERTY_CMD_CONFIG = "cmd.configuration";

    /**
     * init method reads configuration and configures the gpio ports
     */
    @Override
    public void init() {
        log.info("init()");
        String gpioConfigFileString = System.getProperty(SYSTEM_PROPERTY_GPIO_CONFIG);
        if (null == gpioConfigFileString) {
            log.error("System property " + SYSTEM_PROPERTY_GPIO_CONFIG + " is not set!");
        }
        String cmdConfigFileString = System.getProperty(SYSTEM_PROPERTY_CMD_CONFIG);
        if (null == cmdConfigFileString) {
            log.warn("System property " + SYSTEM_PROPERTY_CMD_CONFIG + " is not set!");
        }
        context = SingleContext.getInstance(gpioConfigFileString, cmdConfigFileString);

        cron = new CronjobAdapter(new File(System.getProperty(SYSTEM_PROPERTY_CRON_CONFIG)));
        cron.start();
    }

    /**
     * destroy method from interface. reset all states and unexport all gpio
     * pins
     */
    @Override
    public void destroy() {
        log.info("destroy()");
        cron.stop();
        context.destroy();
    }

    /**
     * send all your pin values to set as parameters, e.g. [GPIO.0.NAME]=[0|1]
     * <p> if pin is setup to read call as [GPIO.0.NAME]=IN <p> the response
     * will send you the state of each requested and known pin as json, or -1 if
     * you tried to set an input pin or tried to read an output pin or -2 if pin
     * is within blocked time
     * <p/>
     * e.g. {"g0":0,"g1":1,"g4":-1}
     *
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        log.info("Get request for " + req.getRequestURI()
            + "?" + req.getQueryString() + " from " + req.getRemoteAddr());

        // set/get hardware values
        ArrayList<SimpleCommand> commandArray = SimpleCommand.createCommandList(req.getQueryString());
        Map<String, String> hwHandledMap = context.handleConditionsAndCommands(commandArray);

        // format response
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        boolean isFirst = true;
        for (String key : hwHandledMap.keySet()) {
            sb.append((isFirst ? "" : ","));
            isFirst = false;
            sb.append("\"").append(key.replaceAll("\"", "\\\\\"")).append("\":");
            sb.append(hwHandledMap.get(key));
        }
        sb.append("}");
        resp.getWriter().println(sb.toString());
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        log.debug("Handle post as get");
        this.doGet(req, resp);
    }
}
