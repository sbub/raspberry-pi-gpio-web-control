/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio;

import de.derbub.rpigpio.cmd.ExternalProcessesManager;
import de.derbub.rpigpio.gpio.ConfiguredGpio;
import de.derbub.rpigpio.gpio.Direction;
import de.derbub.rpigpio.gpio.GpioPin;
import de.derbub.rpigpio.gpio.GpioSysFsInterface;
import de.derbub.rpigpio.gpio.files.FileHandlingFactory;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Singleton class in order to have a configured context from "everywhere", i.e.
 * form Servlet and from Cronjobs
 *
 * @author sb
 */
public class SingleContext {

    private static Logger log = Logger.getLogger(SingleContext.class);
    private static Properties configProperties = new Properties();
    private static GpioSysFsInterface gpioInterface;
    private static SingleContext INSTANCE;
    private static Map<String, String> virtualVariables = new HashMap<String, String>();
    private static Map<String, String> cmdReturnCodes = new HashMap<String, String>();
    public static ExternalProcessesManager extProcessManager;
    public static final String CONFIG_SIMULATE_GPIOS_KEY = "simulate.gpios";
    public static final String CONFIG_TI_MAX_COUNTER_KEY = "ti.max.counter";
    public static final String VIRTUAL_KEY_PREFIX = "VIRTUAL";


    /**
     * private constructor for singleton pattern
     */
    private SingleContext() {
    }

    /**
     * Singleton creator which also initializes all GPIOs
     *
     * @param gpioConfigFileString which holds the name to the gpio config file
     * @param cmdConfigFileString which holds the name to the cmd config file
     * @return an instance of SingleContext
     */
    public static synchronized SingleContext getInstance(String gpioConfigFileString, String cmdConfigFileString) {
        if (INSTANCE != null) {
            throw new RuntimeException("SingleContext already initialized");
        }
        // read configuration file to configProperties
        File gpioConfigFile = new File(gpioConfigFileString);
        try {
            configProperties.load(new FileReader(gpioConfigFile));
        } catch (IOException e) {
            if (null == gpioConfigFileString) {
                log.error("Configuration for gpio not found");
            } else {
                if (!gpioConfigFile.exists()) {
                    log.error("Config file " + gpioConfigFileString + " does not exist");
                }
            }
            log.error(e.getClass().getName() + ":" + e.getMessage());
        }
        // read configuration from configProperties and initialize system
        String simulateGpiosString = configProperties.getProperty(CONFIG_SIMULATE_GPIOS_KEY);
        Boolean simulateGpios = (null == simulateGpiosString
            ? Boolean.TRUE
            : Boolean.valueOf(simulateGpiosString));
        log.warn(CONFIG_SIMULATE_GPIOS_KEY + " is set to " + simulateGpios);

        String tiMaxCounterString = configProperties.getProperty(CONFIG_TI_MAX_COUNTER_KEY);
        int tiMaxCounter = 100000;
        try {
            tiMaxCounter = Integer.parseInt(tiMaxCounterString);
            log.debug(CONFIG_TI_MAX_COUNTER_KEY + " is set to " + tiMaxCounter);
        } catch (NumberFormatException e) {
            log.error(CONFIG_TI_MAX_COUNTER_KEY + " is not a number. Using default (" + tiMaxCounter + ").");
        }

        Map<String, ConfiguredGpio> gpioMap = new HashMap<String, ConfiguredGpio>();
        for (GpioPin gpioPin : GpioPin.values()) {
            Direction direction = Direction.getDirectionByString(
                configProperties.getProperty(gpioPin.getConfigDirectionKey()));
            String name = configProperties.getProperty(gpioPin.getConfigNameKey());
            name = (null == name ? String.valueOf(gpioPin.getNumber()) : name);
            if (null != direction) {
                ConfiguredGpio gpio = new ConfiguredGpio(gpioPin, direction, name,
                    configProperties.getProperty(gpioPin.getConfigDefaultStateKey()),
                    configProperties.getProperty(gpioPin.getConfigAutoToggleTimeKey()),
                    configProperties.getProperty(gpioPin.getConfigBlockTimeKey()));
                ConfiguredGpio oldValue = gpioMap.put(gpio.getUserdefinedName(), gpio);
                log.info("Read configuration for " + gpio);
                if (null != oldValue) {
                    log.warn("Conflicting gpio names: " + oldValue + " is not configured.");
                }
            }
        }
        // setup gpio ports
        gpioInterface = GpioSysFsInterface.getInstance(gpioMap, tiMaxCounter);
        FileHandlingFactory.setSimulateGpios(simulateGpios);
        // setup ports and default values
        if (!gpioInterface.setupAllGpioPorts() || !gpioInterface.setAllDefaultState()) {
            log.warn("Setup error, will cleanup immediately");
            gpioInterface.closeAllGpioPorts();
        }

        extProcessManager = new ExternalProcessesManager(new File(cmdConfigFileString));

        INSTANCE = new SingleContext();
        return INSTANCE;
    }

    /**
     * returns a SingleContext instance or null (if not configured yet)
     *
     * @return an Instance of SingleContext if getInstance was called before, else null
     */
    public static SingleContext getInitializedInstance() {
        return INSTANCE;
    }

    /**
     * methods handles the actions and makes calls to sysfs
     *
     * @param commandList an ArrayList of SimpleCommand
     * @return a Map of gpioNames and the values (either read on input or future values of output ports)
     */
    public synchronized Map<String, String> handleConditionsAndCommands(ArrayList<SimpleCommand> commandList) {
        Map<String, String> returnMap = new HashMap<String, String>();
        SimpleCommand[] commandArray = commandList.toArray(new SimpleCommand[commandList.size()]);
        SimpleCommand cmd;
        for (int i = 0; i < commandArray.length; i++) {
            cmd = commandArray[i];
            if (cmd.isCondition()) {
                // if it is a condition on a virtual key or a command
                if (isVirtualKey(cmd.getKey()) || isCommandKey(cmd.getKey())) {
                    String oldValue = isVirtualKey(cmd.getKey()) ? virtualVariables.get(cmd.getKey()) : cmdReturnCodes.get(cmd.getKey());
                    oldValue = (null == oldValue ? "0" : oldValue);
                    if (cmd.checkCondition(oldValue)) {
                        log.debug("condition '" + cmd.getKey() + " as expected");
                        continue;
                    } else {
                        log.info("condition '" + cmd.getKey() + ": is: " + oldValue + " otherValue: " + cmd.getValue() + ". Will stop now");
                        break;
                    }
                }
                // if it is a condition on a real port
                Map<String, String> singleInputMap = new HashMap<String, String>();
                singleInputMap.put(cmd.getKey(), Direction.CONDITIONAL_INPUT);
                Map<String, String> gpioInputMap = gpioInterface.handleGpioPorts(checkGpioMapValidity(singleInputMap));
                if (null != gpioInputMap && !gpioInputMap.isEmpty()) {
                    String requestedValue = gpioInputMap.get(cmd.getKey());
                    if (null == requestedValue) {
                        log.warn("condition '" + cmd.getKey() + "' requested, but no answer received. Will stop now");
                        break;
                    }
                    if (cmd.checkCondition(requestedValue)) {
                        log.debug("condition '" + cmd.getKey() + " as expected");
                    } else {
                        log.info("condition '" + cmd.getKey() + ": " + requestedValue + "!=" + cmd.getValue() + ". Will stop now");
                        break;
                    }
                } else {
                    log.warn("condition '" + cmd.getKey() + "' unknown will stop now");
                    break;
                }
            } else {  // else no condition
                Map<String, String> map = new HashMap<String, String>();
                // set output ports or read input ports until next condition (keep i correct because surrounding "for") 
                i--;
                while ((i + 1) < commandArray.length && !commandArray[i + 1].isCondition()) {
                    i++;
                    cmd = commandArray[i];
                    if (isVirtualKey(cmd.getKey())) {
                        if (Direction.IN.equals(Direction.getDirectionByString(cmd.getValue()))) {
                            returnMap.put(cmd.getKey(), (null == virtualVariables.get(cmd.getKey()) ? "0" : virtualVariables.get(cmd.getKey())));
                            log.debug("Read " + returnMap.get(cmd.getKey()) + " for " + cmd.getKey());
                        } else {
                            virtualVariables.put(cmd.getKey(), cmd.getValue());
                            returnMap.put(cmd.getKey(), cmd.getValue());
                            log.info("Setting value " + cmd.getValue() + " for " + cmd.getKey());
                        }
                    } else {
                        if (isCommandKey(cmd.getKey())) {
                            if (Direction.IN.equals(Direction.getDirectionByString(cmd.getValue()))) {
                                returnMap.put(cmd.getKey(), (null == cmdReturnCodes.get(cmd.getKey()) ? "0" : cmdReturnCodes.get(cmd.getKey())));
                                log.debug("Read " + returnMap.get(cmd.getKey()) + " for " + cmd.getKey());
                            } else {
                                String retCode = extProcessManager.process(cmd.getKey(), cmd.getValue());
                                cmdReturnCodes.put(cmd.getKey(), retCode);
                                returnMap.put(cmd.getKey(), retCode);
                                log.info("Setting return code value " + retCode + " for " + cmd.getKey());
                            }
                        } else {
                            map.put(cmd.getKey(), cmd.getValue());
                        }
                    }
                }
                returnMap.putAll(gpioInterface.handleGpioPorts(checkGpioMapValidity(map)));
            }
        }
        return returnMap;
    }

    /**
     * @param key String the key to check
     * @return true if key starts with VIRTUAL_KEY_PREFIX
     */
    private boolean isVirtualKey(String key) {
        return key.startsWith(VIRTUAL_KEY_PREFIX);
    }


    /**
     * @param key String the key to check
     * @return true if key starts with CMD_KEY_PREFIX
     */
    private boolean isCommandKey(String key) {
        return key.startsWith(ExternalProcessesManager.CMD_KEY_PREFIX);
    }

    /**
     * method expects userdefined names of gpio keys and values and checks keys
     * and values
     *
     * @param uncheckedMap Map of gpioNames and values.
     * @return Map of only valid key/value combinations (i.e. if you try to set an input port, it will be removed here)
     */
    private Map<String, String> checkGpioMapValidity(Map<String, String> uncheckedMap) {
        Map<String, ConfiguredGpio> configuredGpioMap = gpioInterface.getConfiguredGpioMap();
        // read possible keys
        Map<String, String> nameValuesUncheckedMap = new HashMap<String, String>();
        for (String name : configuredGpioMap.keySet()) {
            String value = uncheckedMap.get(name);
            if (null != value) {
                String oldValue = nameValuesUncheckedMap.put(name, value);
                if (null != oldValue) {
                    log.warn("Conflicting values for " + name + ". " + value + " is used now");
                }
            }
        }
        // check validity of values (for keys)
        Map<String, String> nameCheckedValuesMap = new HashMap<String, String>();
        for (String name : nameValuesUncheckedMap.keySet()) {
            String uncheckedValue = nameValuesUncheckedMap.get(name).trim();
            ConfiguredGpio gpio = configuredGpioMap.get(name);
            if (gpio.getDirection().isInput() &&
                (Direction.IN.equals(Direction.getDirectionByString(uncheckedValue)) || Direction.CONDITIONAL_INPUT.equals(uncheckedValue))
                ) {
                nameCheckedValuesMap.put(name, Direction.IN.getValue());
            }
            if (gpio.getDirection().isOutput()) {
                if (("0".equals(uncheckedValue) || "1".equals(uncheckedValue))) {
                    nameCheckedValuesMap.put(name, uncheckedValue);
                }
                if (Direction.CONDITIONAL_INPUT.equals(uncheckedValue) || Direction.IN.equals(Direction.getDirectionByString(uncheckedValue))) {
                    nameCheckedValuesMap.put(name, Direction.IN.getValue());
                }
            }
            if (gpio.getDirection().isTimeBasedAnalogIn()) {
                // TI is internally handled as out (at least on getting the currentOutputPortState)
                if (Direction.IN.equals(Direction.getDirectionByString(uncheckedValue))) {
                    nameCheckedValuesMap.put(name, Direction.TI.getValue());
                }
                if (Direction.CONDITIONAL_INPUT.equals(uncheckedValue)) {
                    nameCheckedValuesMap.put(name, Direction.IN.getValue());
                }
            }
        }
        return nameCheckedValuesMap;
    }

    /**
     * shutdown hook
     */
    public void destroy() {
        INSTANCE = null;
        gpioInterface.closeAllGpioPorts();
    }
}
