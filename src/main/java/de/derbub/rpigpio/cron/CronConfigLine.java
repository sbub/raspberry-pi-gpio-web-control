/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.cron;

import de.derbub.rpigpio.SimpleCommand;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Helper class which takes a crontab line, checks validity and has
 * some access helpers
 *
 * @author sb
 */
public class CronConfigLine {

    private static Logger log = Logger.getLogger(CronConfigLine.class);
    private String schedule;
    private String command;
    private ArrayList<SimpleCommand> commandList;

    /**
     * constructor is doing the parsing already
     *
     * @param line String one config line
     */
    public CronConfigLine(String line) {
        if (!line.startsWith("#")) {
            String[] lineParts = line.split("::");
            if (null != lineParts && lineParts.length == 2) {
                String tmpSchedule = lineParts[0];
                String tmpCommand = lineParts[1];

                if (null != tmpSchedule && tmpSchedule.trim().length() > 0
                    && null != tmpCommand && tmpCommand.trim().length() > 0) {
                    schedule = tmpSchedule.trim();
                    command = tmpCommand.trim();
                    commandList = SimpleCommand.createCommandList(command);
                } else {
                    log.warn("the cron line parts are ignored _" + line + "_");
                }
            } else {
                log.info("the cron line is ignored _" + line + "_");
            }
        }
    }

    /**
     * checks if line looks valid (actually it has a "::" in the middle)
     * <p/>
     * comment is false, too
     *
     * @return boolean if it is a valid line
     */
    public boolean isValid() {
        return (null != schedule && schedule.length() > 0
            && null != command && command.length() > 0);
    }

    public String getSchedule() {
        return schedule;
    }

    public ArrayList<SimpleCommand> getCommandList() {
        return commandList;
    }

    @Override
    public String toString() {
        return schedule + " | " + command;
    }

}
