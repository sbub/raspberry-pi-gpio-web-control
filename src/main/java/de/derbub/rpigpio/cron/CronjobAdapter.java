/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.cron;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Class handles config read and creation/cleanup of quartz scheduler
 *
 * @author sb
 */
public class CronjobAdapter {

    private static Logger log = Logger.getLogger(CronjobAdapter.class);
    private Scheduler scheduler;
    List<CronConfigLine> cronLines = new ArrayList<CronConfigLine>();

    public CronjobAdapter(File cronConfigFile) {
        System.setProperty("org.terracotta.quartz.skipUpdateCheck", "true");

        log.debug("reading file " + cronConfigFile);
        BufferedReader bufferedReader = null;
        if (cronConfigFile.exists()) {
            try {
                bufferedReader = new BufferedReader(new FileReader(cronConfigFile));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    CronConfigLine cronConfigLine = new CronConfigLine(line);
                    if (cronConfigLine.isValid()) {
                        cronLines.add(cronConfigLine);
                    }
                }
            } catch (FileNotFoundException fex) {
                log.error("file does not exists: " + cronConfigFile + "(Msg:" + fex.getMessage() + ")");
            } catch (IOException ex) {
                log.error("Got IOException while reading " + cronConfigFile + "(Msg:" + ex.getMessage() + ")");
            } finally {
                try {
                    if (null != bufferedReader) {
                        bufferedReader.close();
                    }
                } catch (Exception e) {
                    // catch away
                }
            }
        } else {
            log.info("no cronjobs because file does not exists: " + cronConfigFile);
        }
    }

    public void start() {
        try {
            if (!cronLines.isEmpty()) {
                scheduler = StdSchedulerFactory.getDefaultScheduler();
                scheduler.start();

                for (CronConfigLine ccl : cronLines) {
                    JobDetail job = newJob(GPIOOutJob.class).build();
                    job.getJobDataMap().put(GPIOOutJob.COMMAND_KEY, ccl);
                    CronTrigger trigger = newTrigger().withSchedule(cronSchedule(ccl.getSchedule())).build();
                    scheduler.scheduleJob(job, trigger);
                    log.info("Scheduled job: " + ccl);
                }
            } else {
                log.debug("no cronjobs configured");
            }
        } catch (SchedulerException e) {
            log.error("starting scheduler failed with message " + e.getMessage());
        }
    }

    public void stop() {
        try {
            if (!cronLines.isEmpty()) {
                scheduler.shutdown();
                log.info("stopping scheduler");
            }
        } catch (SchedulerException e) {
            log.error("stopping scheduler failed with message " + e.getMessage());
        }
    }
}
