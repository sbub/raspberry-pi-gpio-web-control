/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio.cron;

import de.derbub.rpigpio.SingleContext;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;

/**
 * class implements a quartz job
 *
 * @author sb
 */
public class GPIOOutJob implements Job {

    public static final String COMMAND_KEY = "key";
    private static Logger log = Logger.getLogger(GPIOOutJob.class);

    public GPIOOutJob() {
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {
        CronConfigLine ccl = (CronConfigLine) context.getJobDetail().getJobDataMap().get(GPIOOutJob.COMMAND_KEY);
        Date now = new Date();
        log.info("execute() [" + now + "]: " + ccl);
        try {
            SingleContext.getInitializedInstance().handleConditionsAndCommands(ccl.getCommandList());
        } catch (Exception e) {
            log.error(e.getClass().getName() + ": " + e.getMessage(), e);
        }
    }
}
