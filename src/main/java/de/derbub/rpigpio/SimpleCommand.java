/*
 *
 * Copyright 2012 der-bub.de
 * http://www.der-bub.de
 * Author: Sebastian Bub (sebastian@der-bub.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.derbub.rpigpio;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * JavaBean to hold all values of a command
 *
 * @author sb
 */
public class SimpleCommand {

    /**
     * enum for condtion
     */
    public enum Condition {

        NO_CONDITION("="), EQUAL("=="), NOT_EQUAL("!="), GREATER(">"), GREATER_EQUAL(">="), LESS("<"), LESS_EQUAL("<=");
        private String conditionString;

        private Condition(String conditionString) {
            this.conditionString = conditionString;
        }

        public static Condition getConditionByString(String s) {
            if (null != s) {
                String trimmedString = s.trim();
                for (Condition c : Condition.values()) {
                    if (trimmedString.equals(c.toString())) {
                        return c;
                    }
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return conditionString;
        }
    }

    public static final Pattern CONDITION_OR_ASSIGN_PATTERN = Pattern.compile("^(.*?)(==|!=|>=|>|<=|<|=)(.*)");
    private static Logger log = Logger.getLogger(SimpleCommand.class);

    private String key;
    private String value;
    private Condition condition;

    public SimpleCommand(String key, String value, String conditionString) {
        this.key = key;
        this.value = value;
        this.condition = Condition.getConditionByString(conditionString);
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public boolean isCondition() {
        return condition != null && !condition.equals(Condition.NO_CONDITION);
    }

    public boolean checkCondition(String otherValue) throws NumberFormatException {
        try {
            int intValue = Integer.parseInt(value);
            int intOtherValue = Integer.parseInt(otherValue);
            switch (condition) {
                case EQUAL:
                    return (intOtherValue == intValue);
                case NOT_EQUAL:
                    return (intOtherValue != intValue);
                case GREATER:
                    return (intOtherValue > intValue);
                case GREATER_EQUAL:
                    return (intOtherValue >= intValue);
                case LESS:
                    return (intOtherValue < intValue);
                case LESS_EQUAL:
                    return (intOtherValue <= intValue);
                default:
                    log.warn(this.getClass().getName() + " is not a condition: " + this);
            }
        } catch (NumberFormatException nfe) {
            log.warn(this.getClass().getName() + ": value is not a number: " + this + "(Exception message: " + nfe.getMessage() +")");
        }
        return false;
    }

    @Override
    public String toString() {
        return "key: " + key + ", value: " + value + ", condition: " + condition;
    }


    /**
     * method interpretes GET Parameter style commands (from web or cron)
     *
     * @param commandString as GET parameters (watch out, may contain ==
     * @return an ordered List of commands
     */
    public static synchronized ArrayList<SimpleCommand> createCommandList(String commandString) {
        ArrayList<SimpleCommand> tmpList = new ArrayList<SimpleCommand>();
        for (String param : commandString.split("&")) {
            log.debug("read in " + param);
            Matcher m = CONDITION_OR_ASSIGN_PATTERN.matcher(param);
            m.matches();
            String key, value, condition;
            try {
                key = URLDecoder.decode(m.group(1), "UTF-8");
                condition = m.group(2);
                value = URLDecoder.decode(m.group(3), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                log.warn("received an UnsupportedEncodingException on key or value for " + param + ". Trying to work on it undecoded.");
                key = m.group(1);
                condition = m.group(2);
                value = m.group(3);
            } catch (IllegalStateException ise) {
                log.warn("ignoring command _" + param + "_ Regexp does not match.");
                continue;
            }
            SimpleCommand tmpCmd = new SimpleCommand(key, value, condition);
            tmpList.add(tmpCmd);
            log.debug("createCommandList() add new SimpleCommand(" + tmpCmd + ")");
        }
        return tmpList;
    }

}
